# HTML

## ¿Que es HTML?

HTML es la abreviación de *Hypertext Markup Language* y es el lenguaje que le da la estructura a una página web. 

Con HTML podemos crear el esqueleto que va a contener toda la informacion, en caso de que sea una página estática o bien, en donde se va a ir mostrando la informacion que este en una base de datos en caso de que sea una página dinámica.

A este archivo que contiene HTML luego podremos darle forma y color con CSS o funcionalidades con JS, pero eso lo vamos a ver mas adelante.

## Anatomía de una página web

![estructura-web](/mnt/tomas/code/Doc/HTML y CSS/assets/estructura-web.jpg)

### Header (Cabecera)

El *header* o *cabecera* es en donde vamos a tener una *barra de navegacion* o *navbar* que nos permitirá movernos por toda el sitio web. También puede contener el logo.

### Main content (Contenido)

Es la estructura o informacion principal de nuestra página web

### Sidebar

Suele ser contenido secundario, como publicidad o cosas relacionadas a lo que estas viendo.

### Footer (Pie de página)

Puede haber informacion de contacto entre otras cosas, y dependiendo la estructura general puede ser muy pequeño o muy visible.

## Primer archivo HTML

### Que es una etiqueta

La estructura de un archivo HTML se define mediante etiquetas. Existen **dos tipos de etiquetas**, las que tienen etiqueta de cierre y las que no. 

``` html
<body> <!-- Etiqueta de apertura -->
  <!-- Contenido -->
</body> <!-- Etiqueta de cierre -->
```

``` html
<meta name="description" content="Descripcion breve del sitio" />
<!-- Etiqueta sin cierre -->
```

### Estructura basica

Hay algunas cosas a tener en cuenta respecto a la estructura de un archivo HTML.

La estructura basica es

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    ...
  </head>
  <body>
  	<!-- Contenido -->
  </body>
</html>
```

La etiqueta *doctype* es lo que le dice al navegador "Ey, este archivo es un archivo HTML".

La etiqueta *html* es el padre de todas las etiquetas y tiene un atributo que le informa al navegador en que lenguaje esta escrito la informacion que el usuario va a visualizar.

La etiqueta *head* contiene **propiedades e informacion que el navegador o internet necesita**. Puede ser una descripcion del sitio, que set de caracteres se van a utilizar, y tambien los enlaces a otros archivos (Algo asi como importar un archivo). Todo lo que este dentro de la etiqueta *head* **no sera visible para el usuario**

La etiqueta *body* es la que **va a contener todo lo que es visible para el usuario**. Listas, tablas, imagenes, parrafos, titulos, etc.

### Estructura completa

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    ...
  </head>
  <body>
    <header>
    	<nav></nav>
    </header>
    
    <main>
    	<section>
     		<article></article>
        <article></article>
      </section>
    </main>
    
    <footer></footer>
  </body>
</html>
```

En caso de querer indagar en las etiquetas explicadas o en otras etiquetas, este sitio es un buen lugar para buscar.

https://htmlreference.io/

### Anatomia de una etiqueta

![anatomia-de-una-etiqueta](/mnt/tomas/code/Doc/HTML y CSS/assets/anatomia-de-una-etiqueta.png)

---

## Objetos multimedia

### Imagenes

#### Formato

Antes de empezar con este tipo de etiquetas, debemos aprender un poco acerca de las imágenes.

Hay varios formatos de imagenes que se pueden utilizar en internet dependiendo de las circunstancias. Lo importante a saber es que algunos formatos **tienen perdida de calidad y otros no**. Esto es relevante ya que si una imagen se ve muy bonita, probablemente su tamaño sea muy grande y cargar la página lleve mas tiempo de lo esperado, con lo cual esto va a terminar en un incomodo y largo tiempo de espera (Que es una pesima experiencia de usuario).

A continuacion, nos vamos a referir a las imagenes que tienen perdida como **Lossy** y los que no tienen como **Lossless**.

![formato-imagenes](/mnt/tomas/code/Doc/HTML y CSS/assets/formato-imagenes.png)

*Nota: Muchos colores implica que la imagen es pesada, y al comprimirla, varios de estos colores se van a perder.*

#### Tamaño

El tamaño promedio de una imagen es de 70KB. Un tamaño mayor implica aumentar los tiempos de espera para que la página cargue completamente. Ahora, como logramos que una imagen tenga ese tamaño?

- Para comprimir la imagen
  - TinyPNG (https://tinypng.com/)
- Para quitar metadatos
  - Verexif (https://www.verexif.com/en/)

## Etiquetas

### Etiquetas multimedia

Las etiquetas multimedia son etiquetas que permiten insertar imagenes, musica, videos, etc, en nuestra página web.

#### Insertando una imagen

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <figure>
      <img
        src="./pics/cat"
        alt="Imagen de un gatito"
      />
      <figcaption>Mi gato intentando dormir</figcaption>
    </figure>
  </body>
</html>
```

Desde HTML5, la etiqueta *img* **debe ser hija de una etiqueta *figure*** ya que dentro de *figure* tendremos disponible *figcaption* que es un pie de foto. 

Dentro de la etiqueta *img* se encuentran dos atributos necesarios, estos son **source** y **alt**. source es la ruta en donde se encuentra la imagen y alt es el texto alternativo que se muestra cuando no se encuentra la imagen.

El atributo *alt* debera estar siempre presente ya que es una utilidad sobre todo para las personas con discapacidades visuales. El contenido de *alt* debera ser un texto descriptivo de la imagen.

#### Insertando un video

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <video preload="auto" controls>
      <source src="./video-de-prueba.mkv" />
    	<source src="./video-de-prueba.mp4" />
    </video>
  </body>
</html>
```

Para insertar un video se utiliza la etiqueta *video* la cual lleva 2 atributos principales. 

El primer atributo es *preload=auto* , este atributo le dice al navegador que **cargue el video mientras carga la página web**. De esta manera, cuando la página ya esté cargada, el usuario puede **reproducir el video sin demasiada demora**.

El segundo atributo es *controls* y este atributo simplemente **agrega una interfaz para reproducir el video**. Es decir, agrega un botón de *play* y *pause*, un *timeline* del video, etc.

Por ultimo, dentro de la etiqueta *video*, aparecen nuevas etiquetas. Estas etiquetas *source* **tienen el objetivo de proporcionar distintos videos** en caso de que no pueda reproducir alguno, ya sea por que no puede reproducir algún formato de video o bien, porque no lo encuentra. 

En el ejemplo de mas arriba vemos que la primer etiqueta *source* está importando un video con formato MKV. Lo mas probable es que el navegador no lo pueda reproducir, con lo cual, va a buscar otra etiqueta *source* que si pueda reproducir. En este caso, va a reproducir el video con formato MP4 que es mas común.

---

### Formularios

#### ¿Que es un formulario?

Un formulario es un elemento de la página web con el objetivo de recibir o enviar informacion al usuario.

Un ejemplo podria ser un formulario para registrarse en un sitio web, o para adquirir un producto en una tienda virtual.

Los formularios tiene la cantidad de etiquetas suficientes como para tener su propia categoría, ahora vamos a profundizar en eso.

Los formularios no aportan una buena experiencia de usuario, siempre es mejor evitarlos o hacerlos lo mas corto posible. A nadie le gusta rellenar formularios, es aburrido



> **El mejor formulario, es cuando no lo hay** 
>
> *El usuario*



#### Form

La etiqueta *form* tiene como objetivo agrupar todas las etiquetas que van a recolectar información del usuario.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
      <!-- Contenido -->
    </form>
  </body>
</html>
```



#### Input

La etiqueta *input* es justamente un elemento que permite obtener una entrada de información de parte del usuario.

Dentro de la etiqueta *input* se puede definir el tipo de información que va a recibir.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
      <label for="name">
        <span>¿Cual es tu nombre?</span>
      	<input type="text" id="name" placeholder="Tu nombre"/>
      </label>
      
      <label for="birthdate">
        <span>¿Cual es tu fecha de nacimiento?</span>
      	<input type="date" id="birthdate"/>
      </label>
      
      <label for="time-study">
        <span>¿A que hora sueles estudiar?</span>
      	<input type="time" id="time-study"/>
      </label>
    </form>
  </body>
</html>
```



A modo de ejemplo, solo vamos a ver algunos tipos de inputs

Como se vé en el ejemplo, notamos que cada etiqueta *input* está dentro de una etiqueta *label*, **esto es con fines semanticos** y de manera que podamos poner un titulo a cada campo. Cada label tiene un atributo *for* que es para que el navegador sepa a que input pertenece. Notar que el atributo *for* tiene el mismo nombre que el atributo *id* que tiene el input correspondiente (Tiene que ser único).

En cada *input* vemos que tiene el atributo *type* que **le informa al navegador que tipo de dato va a admitir**. Si es una fecha, nos va a mostrar un calendario. Si es un horario, solo va a permitir elegir una hora con un formato definido y **no va a permitir que ingreses una letra**, por ejemplo.

#### Calendar

Existen dos formas de renderizar un input de manera que el usuario pueda elegir una fecha y un horario. Esto puede servir por ejemplo para una página web que permita reservar vuelos o transporte para un viaje a un determinado lugar.

La primer forma es la siguiente

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
      <label for="time">
        <span>Hora</span>
        <input type="time" id="time" name="time" />
      </label>
      
      <label for="Día">
        <span>Hora</span>
        <input type="date" id="day" name="day" />
      </label>
      
      <label for="week">
        <span>Semana</span>
        <input type="week" id="week" name="week" />
      </label>
      
      <label for="month">
        <span>Mes</span>
        <input type="month" id="month" name="month" />
      </label>
      
      <input type="submit" />
    </form>
  </body>
</html>
```

Como vemos, hay bastante código pero cuando querramos almacenar esto en una base de datos, vamos a obtener cada dato por separado . Personalmente esta forma no me convence, ya que resulta mucho código.

Solo me parece útil en caso de necesitar la semana.

Notar que hay un atributo nuevo, el atributo **name** y un **type** nuevo, que contiene la palabra *submit*. 

El input *submit* lo que hace es **enviar el contenido de cada input dentro del formulario**. Esto es con el objetivo de enviar la información para almacenarla en una base de datos o darle una respuesta en función de lo que el usuario puso en algún input.

El atributo **name** es **el nombre de la variable en donde va a viajar el contenido del input**. Por ejemplo, cuando yo elija una hora determinada y aprete el botón *submit*, se va a enviar una variable de nombre *time* y va a contener el horario que yo elegí al momento de apretar el botón *submit*.

Entonces, cuando yo quiera captar el horario que eligio el usuario, mediante JavaScript por ejemplo, voy a buscar la variable *time* y dentro de esta variable va a estar la hora que yo ingresé.

*Nota: El atributo **name** debe ser igual al atributo **id** y **label**.*

La otro forma de generar un calendario, es así

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
      <label for="calendar">
        <span>Calendario</span>
        <input type="datetime" id="calendar" name="calendar" />
      </label>

      <input type="submit" />
    </form>
  </body>
</html>
```

Acá se puede ver que el código se achicó bastante. La única contra de hacerlo de esta manera es que no vamos a saber en que semana es. Pero eso es lo único.

#### Autocomplete y require

Existen funcionalidades extras que se pueden asociar mediante atributos específicos a los inputs. 

##### Autocomplete

El usuario ingresa datos en distintas páginas web, y el usuario pudo haber rellenado un formulario en el que algún input sea igual al que tiene que responder en nuestra página web.

El navegador guarda la información del usuario y nosotros le podemos decir que, si mi página web le pregunta algo al usuario (mediante algún input), y el navegador sabe la respuesta a mi pregunta, que **la rellene por el usuario**.

También es importante saber que el navegador no comparte esta información con nosotros por cuestiones de seguridad, ya que puede almacenar nombres, apellidos, correos, números de tarjetas, etc.

Pero, si tengo un *input* que esta pidiendo el nombre del usuario, el navegador como sabe que debe rellenarlo con el nombre y no con el correo? Bueno, esto el navegador lo sabe porque nosotros **se lo especificamos en el atributo *autocomplete***. Existen muchos valores posibles para el atributo *autocomplete*.

En este link los pueden encontrar

https://developer.mozilla.org/es/docs/Web/HTML/Atributos/autocomplete

##### Require

El atributo **require** no permite que se envíe la información del formulario **a menos que los input que tengan este atributo esten completados**.

Supongamos que tengo un formulario con tres *inputs*, nombre, apellido y código postal. En este formulario, el código postal es obligatorio, no se puede dejar en blanco. Por lo tanto, le colocamos el atributo **require**. 

Si rellenamos los campos nombre y apellido, pero dejamos el código postal en blanco, **el navegador no va a permitir que enviemos la información**, ya que falta el código postal. 

Se va a enmarcar en rojo el input que no esté rellenado y le va a indicar al usuario que se necesita rellenar ese campo.

#### Select

A veces **el usuario tiene que elegir una opción entre una lista de elementos**, por ejemplo, el país en donde vive.

Para estos casos existe la etiqueta *select*.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
      <select name="pais" id="pais">
        <option value="Argentina">Argentina</option>
        <option value="España">Argentina</option>
        <option value="Canadá">Argentina</option>
        <option value="Japón">Argentina</option>
      </select>
    </form>
  </body>
</html>
```

Dentro de la etiqueta *select*, cada opción que deseemos renderizar. 

*Nota: el atributo **value** debe ser igual que el contenido de la etiqueta **option**.*

Esta forma **solo se recomienda cuando hay pocas opciones**, ya que si tenemos 200 opciones, hay que hacer mucho scroll para poder encontrar el valor.

Para casos de listas largas, tenemos la etiqueta *datalist*.

La etiqueta *datalist* funciona de la siguiente manera

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
		...
  </head>
  <body>
    <form>
     	<input type="list" />
      <datalist name="pais">
        <option value="Argentina"></option>
        <option value="España"></option>
        <option value="Canadá"></option>
        <option value="Japón"></option>
      </select>
    </form>
  </body>
</html>
```

Esta etiqueta debe contener varias etiquetas *option* ya que serán las que verá el usuario renderizadas.

La gran diferencia entre *select* y *datalist* es que ***datalist* permite tipear en el cuadro del input y filtrar en base a lo que se escribe**.

Por ejemplo, si uno tiene que buscar su país, no hace falta buscarlo scrolleando o buscando alfabeticamente.

Simplemente hace click en el input y empieza a tipear su país. Entonces la lista se va achicando según las opcione van haciendo match con la palabra ingresada en el input.

*Notar que es **un requisito fundamental tener un input con el atributo type="list"**.*

#### Input vs Button

Resumidamente, podemos utilizar ambos dentro de los formularios. Fuera de los formularios solo podemos usar la etiqueta *button*. 

Usualmente se utiliza la etiqueta *button* con el atributo *type="submit"* cuando queremos capturar ese evento con JavaScript.

Fuera de eso no hay diferencia.