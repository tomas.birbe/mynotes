# Introduccion al mundo web

## Frontend

### Overview

El **Frontend** es el desarrollador que se va a encarga de **lo que sucede en el cliente** (Navegadores we)

### Tecnologias

#### Estandares

- HTML
- CSS
- JavaScript

#### Frameworks CSS

- Bootstrap
- Bulma
- Tailwind

#### Framework y librerias JS

- Angular
- React
- Vue

#### Preprocesadores CSS

- Sass
- Less
- Stylus

#### Compilador o Empaquetador JS

- Babel
- Webpack

## Backend

### Overview

El **Backend** trabaja del lado del **servidor**, desarrollando toda la logica para traer un determinado recurso de un sitio web.

### Tecnologias

#### Lenguajes de programacion

- NodeJS
- Python
- PHP
- Go
- Ruby
- Java
- .NET

#### Frameworks

- Django (Python) 
- Laravel (PHP)
- Rails (Ruby)
- Express (NodeJS)
- Spring (Java)

#### Infraestructura (DevOps)

- Google cloud
- Digital Ocean
- AWS
- Heroku

#### Bases de datos

- MongoDB 
- MySQL 
- PostgreSQL 

## Fullstack

### Overview

El **Fullstack** es el desarrollador que sabe tanto de **Frontend** como **Backend** pero no necesariamente al 100% ya que la evolucion de la tecnologia avanza demasiado rapido como para incorporar cada tecnologia nueva. Dadas las habilidades del **Fullstack**, puede entender como va funcionar el proyecto desde la fase inicial hasta la fase final, pudiendo implementar soluciones tanto en el **Frontend** como en el **Backend**. 

Es importante saber que todo desarrollador **Fullstack** tiene un area de especializacion, siendo esta **Frontend** o **Backend**.

---

## Paginas Dinamicas vs Estaticas

### Paginas estaticas

Tambien conocidas como *Paginas informativas* o *Landing Pages*. Son paginas web en donde la informacion se encuentra en los mismo archivos de la pagina web, es decir, **no se comunican con un base de datos** ya que no hay cambios en la informacion.

*Ej: Una pagina de presentacion para una empresa.*

### Paginas dinamicas

Tambien conocidas como *WebApps*. A diferencias de las paginas estaticas, estas si se conectan contra una base de datos y **tienen mucha mas logica**. Ademas, su informacion puede variar frecuentemente.

*Ej: Twitter, Facebook, YouTube.*