# React

## Apunte de React

### Documentacion y ejercicios

- https://reactjs.org/docs/getting-started.html
- https://www.youtube.com/watch?v=wTpuKOhGfJE&list=PLV8x_i1fqBw0Kn_fBIZTa3wS_VZAqddX7
- https://www.youtube.com/watch?v=T_j60n1zgu0&list=PLV8x_i1fqBw0B008sQn79YxCjkHJU84pC
- https://devdocs.io/react/
- https://devhints.io/react
- https://freecodecamp.org

### Preparando el entorno

- IDE
  - VSCode
  - Atom
  - Sublime Text
  - Brackets
  - CodeSandbox (Editor online)
- NodeJS
- NPM
- React DevTools (https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

### Creando la estructura del proyecto

Para ello vamos ejecutar en la consola

```  npx create-react-app "nombre-del-proyecto"```  

Una vez ejecutado el comando, vamos a ver que se creo un a carpeta con el nombre del proyecto y adentro hay una estructura para empezar a desarrollar.

Para "compilar" el proyecto y visualizarlo en la web, vamos a ejecutra el siguiente comando

``` npm start```

Eso nos va a devolver una ip que es donde se va a renderizar el proyecto. Simplemente ponemos esa ip en el navegador y listo

#### Archivos y carpetas

Dentro del proyecto, vamos a ver algo asi

- node_modules
- public
- src
  - index.js
  - App.css
  - App.js



En *node_modules* estan todos los modulos que instalemos mediante npm.

En *public* es lo donde se va a transpilar todo el codigo de react luego de que lo compilemos para subirlo a un hosting

En *src* esta lo que mas nos interesa de momento. Hay muchos archivos pero de momento solo vamos a utilizar 3. Esta carpeta contiene a *index.js* que es donde se va a montar todo el proyecto y esta el setup del mismo. 

Luego esta *App.css* que es donde van a estar los estilos de nuestra app. Por ultimo, esta *App.js* que es lo que *index.js* va a montar y donde va a estar la base de nuestro proyecto.

### Hola mundo!

Para hacer el iconico "Hola mundo!", nos vamos a ir al App.js y hacemos algo asi

``` react
const App = () => {
  return (
    <div className="App">
      <p>Hola mundo!</p>
    </div>
  );
}

{/* Tambien se pueden usar variables, por ejemplo*/}

const msg = "Hola mundo!"

const App = () => {
  return (
    <div className="App">
      <p>{msg}</p>
    </div>
  );
}


export default App;

{/* Es equivalente a */}

function App = {..}
```



Vamos al navegador, ponemos la ip que nos solto la consola, y veremos los resultados!

### Componentes

Un componente es una porcion de codigo reutilizable que forma parte de un componente mayor a el.

Digamos que un boton, un campo, o un checkbox es un componente hijo de un formulario que a su vez, es componente del layaout de nuestra aplicacion.

#### Crear y renderizar componentes

```react
const HelloWorld = () => {
	return( <p>Hola mundo!</p> )    
}

const App = () => {
  return (
    <div className="App">
      <HelloWorld></HelloWorld>
    </div>
  );
}

export default App;
```

Es importante que, a la hora de crear un componente, el nombre este escrito en mayuscula. Esto es porque podemos crear un componente que se llame "img", entonces cuando lo llamemos react no va a entender si estamos llamando a un componente o una etiqueta de html.

### Propiedades reactivas

En caso de que queramos hacer un contador y con un boton ir aumentando el valor de una variable, nos veriamos tentados a definir una variable y querer modificarla mediante una funcion, por ejemplo



```react
let counter = 0

const incrementCounter = () => {
    counter++
    console.log(counter)
}

const App = () => {
  return (
    <div className="App">
      <p>{counter}</p>
      <button onClick={incrementCounter} > Incrementar </button> {/* onClick es una forma de conectar un boton con una funcion para que se ejecute al hacerle click. Esto lo veremos mas adelante en "manejo de eventos" */}
    </div>
  );
}

export default App;
```

Por ahora ignoremos algunas cosas del codigo.

Si copiamos esta porcion de codigo y lo ejecutamos, vamos a ver que al apretar el boton "incrementar" no va a ocurrir **nada**. Podriamos pensar que es un problema de la logica con la que lo cree, pero en realidad es una caracteristica de React.

Si vamos a la consola del navegador, podemos ver que la variable *counter* si se esta incrementando realmente, pero nosotros no lo vemos ya que React no lo esta renderizando.

Esto es porque React solo renderiza bajo dos circunstancias

- Cuando el **estado** cambia

- Cuando un componente recibe nuevas propiedades del **estado**

  

### Estado

El estado hace referencia a la informacion que almacena un componente, o varios en caso de ser un estado central.

Nosotros deberemos definir el estado de cada componente de la siguiente manera

```react
import { useState } from 'react'

const App = () => {
  const [ counter, setCounter ] = useState(0) {/*El primer elemento del array es el nombre de la variable, el segundo es la funcion que utilizamos para cambiar el valor de la variable, y el valor que le pasamos a "useState" es el valor inicial de la variable*/}
  
  const incrementCounter = () => {
      setCounter(counter + 1) {/* Es importante no alterar el valor de una propiedad de lestado por fuera de su funcion definida, ya que podria causar errores. Por eso no podemos hacer counter++*/}
  }
    
  return (
    <div className="App">
      <p>{counter}</p>
   	 <button onClick={incrementCounter}></button>
    </div>
  );
}

export default App;

```



Si copiamos este codigo y lo renderizamos vamos a ver que ahora si se vuelve una propiedad reactiva, ya que cada vez que apretamos el boton "incrementar", se actualiza el valor del estado y por ende React vuelve a renderizar la app. 

Lo interesante de esto es que React utiliza **DOM virtual** (lo veremos mas adelante) y esto permite que solo actualice lo que se cambia, no todo el html. 

Esto se traduce a mayor flexibilidad y velocidad en los tiempos de carga y descarga.

### Comunicacion entre componentes

#### Padre a hijo

Para enviar informacion de un componente padre a un componente hijo, utilizaremos las props.

```react
const HelloWorld = (props) => { 
  return(
    <p>{props.msg}</p>
  )
}

{/* Podemos utilizar el destructuring de js, y directamente utilizar la propiedad. Esto es incluso mejor practica para que el codigo sea mas limpio*/}

{/*
    const HelloWorld = ({msg}) => { 
      return(
        <p>{msg}</p>
      )
	}	
*/}


const App = () => {
  return (
    <div className="App">
      <HelloWorld msg="Hola mundo!"/>
      <img src="" alt=""/>
    </div>
  );
}

export default App;
```

